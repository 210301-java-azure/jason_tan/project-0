# Electronic Medical Records server
---
## Endpoints

##### POST /register
This is for creating user accounts. Accounts can be of either the `patient` type or the `admin` type

##### POST /login 
This is for logging into existing user accounts with the associated username and password

##### /patients
When making any sort of request to an endpoint of this type, the existence of an authorization token is checked. If there is no token found, the user is denied access.

##### GET /patients/search 
This request can be performed in three different modes based on the `searchMode` form parameter. `all` will get the information of all patients in the system and is only permitted for admin use. `name` will get patient information by last and first name. This is also for admin use only. `id` will retrieve patient information based on the id of the user account. This can be performed by `patient` accounts however only for their own information. If they attempt to search for patient information with a user id not equal to their own user id, they will be denied access.
```json
{
    "uid": 8,
    "firstName": "John",
    "lastName": "Doe",
    "sex": "M",
    "dob": "06/16/2000",
    "race": "White",
    "address": "1235 something blvd",
    "city": "cityname",
    "state": "FL",
    "phone": "(123)456-7890",
    "insuranceName": "BlueCross",
    "insurancePolicyNumber": 3655369,
    "insuranceGroupID": 4722884
}
```
##### POST /patients/add 
This request allows users to enter patient information into the system. A `patient` user can only add information for their own account. 

##### PATCH /patients/edit
This request allows users to edit patient information that already exists in the database. This is done based on first and last name. A `patient` user can only make changes to patient information associated with their own account.

##### DELETE /patients/delete
This request allows `admin` users to delete patient information based on first and last name.

## ERD

<img src="Screenshot 2021-03-19 105654.png">
