package dev.tan.controllers;

import dev.tan.data_access.Database;
import dev.tan.models.User;
import io.javalin.http.BadRequestResponse;
import io.javalin.http.Context;
import io.javalin.http.UnauthorizedResponse;
import org.slf4j.*;
import io.jsonwebtoken.*;
import io.jsonwebtoken.security.*;
import java.security.Key;

public class AuthController {

    private static Logger logger = LoggerFactory.getLogger(AuthController.class);
    private static Database db = new Database();

    public static void handlePreAuthCheck(Context ctx) {
        logger.info("checking for authorization");
        String token = ctx.header("Authorization");
        logger.info(token);
        if(token == null || token.equals(""))
            throw new UnauthorizedResponse("Not authorized to perform this action. Please log in and try again.");

    }

    public static void handleCreateAccount(Context ctx) {
        String username = ctx.formParam("username");
        String password = ctx.formParam("password");
        String role = ctx.formParam("role");
        int res = db.addAccount(username, password, role);
        if(res > 0) {
            logger.info("Account created successfully");
            ctx.status(200);
            ctx.result("Account created successfully\nUser ID: "+res);
        }
        else {
            logger.info("username taken");
            throw new BadRequestResponse("Username already taken");
        }
    }

    public static void handleLogin(Context ctx) {
        String username = ctx.formParam("username");
        String password = ctx.formParam("password");
        if(username != null && username.equals("") ||
           password != null && password.equals("")) throw new BadRequestResponse();
        logger.info("attempting logging in as: " + username);
        User user = db.getUser(username, password);
        if(user != null) {
            logger.info("logged in as " + user);

            ctx.header("Authorization",
                    (user.getRole()==1 ? "admin" : "patient")+","+user.getId());
            ctx.status(200);
            ctx.result("Successfully logged in as " + user.getUsername());
        }
    }
}
