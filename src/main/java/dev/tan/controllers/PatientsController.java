package dev.tan.controllers;

import dev.tan.data_access.Database;
import dev.tan.models.Patient;
import io.javalin.http.BadRequestResponse;
import io.javalin.http.Context;
import io.javalin.http.UnauthorizedResponse;
import org.slf4j.*;

import java.util.List;

public class PatientsController {

    private static Logger logger = LoggerFactory.getLogger(PatientsController.class);
    private static Database db = new Database();

    public static void handleSearchPatients(Context ctx) {
        String mode = ctx.formParam("searchMode");
        String[] token = ctx.header("Authorization").split(",");
        if(mode == null) throw new BadRequestResponse("Please provide a search parameter");
        List<Patient> res = null;
        switch (mode) {
            case "all":
                if(!token[0].equals("admin"))
                    throw new UnauthorizedResponse("This request requires admin privileges");
                logger.info("getting all patients");
                res = db.searchPatients("all", "", "");
                break;
            case "name":
                if(!token[0].equals("admin"))
                    throw new UnauthorizedResponse("This request requires admin privileges");
                String lastName = ctx.formParam("lastName");
                String firstName = ctx.formParam("firstName");
                if(lastName == null || firstName == null) throw new BadRequestResponse("Please provide a full search query.");
                logger.info("searching for patients by name: " +
                        lastName + ", " + firstName);
                res = db.searchPatients("name", lastName, firstName);
                break;
            case "id":
                String id = ctx.formParam("id");
                logger.info("searching for patient by id: " + id);
                res = db.searchPatients("id", id, "");
                break;
        }

        if(res.isEmpty()) {
            ctx.result("No patients found");
        }
        else {
            if(token[0].equals("admin")) ctx.json(res);
            else if(token[0].equals("patient")) {
                if(Integer.parseInt(token[1]) != res.get(0).getUid())
                    throw new UnauthorizedResponse("Searching for information of other patients requires admin privileges");
                ctx.json(res);
            }
        }
    }

    public static void handleAddPatient(Context ctx) {
        Patient p = ctx.bodyAsClass(Patient.class);
        String[] token = ctx.header("Authorization").split(",");
        if(token[0].equals("patient") && p.getUid() != Integer.parseInt(token[1]))
            throw new UnauthorizedResponse("Without admin privileges, you are only allowed to add information for yourself");
        logger.info("adding patient");
        logger.info(p.toString());

        if(!db.addPatient(p)) {
            ctx.status(500);
            ctx.result("Could not add patient");
        }
        else {
            ctx.status(200);
            ctx.result("Added patient successfully");
        }
    }

    public static void handleEditPatient(Context ctx) {
        String[] token = ctx.header("Authorization").split(",");
        int uid = Integer.parseInt(token[1]);
        String lastName = ctx.formParam("lastName");
        String firstName = ctx.formParam("firstName");
        String field = ctx.formParam("field");
        String newValue = ctx.formParam("newValue");

        if(db.editPatient(lastName, firstName, field, newValue, uid))
            ctx.result("Patient information changed successfully");
        else
            ctx.result("Could not change patient information");
    }

    public static void handleDeletePatient(Context ctx) {
        String[] token = ctx.header("Authorization").split(",");
        if(!token[0].equals("admin"))
            throw new UnauthorizedResponse("This request requires admin privileges");
        String lastName = ctx.formParam("lastName");
        String firstName = ctx.formParam("firstName");
        logger.info("deleting patient: "+lastName+", "+firstName);

        if(db.deletePatient(lastName, firstName))
            ctx.result("Patient information deleted successfully");
        else
            ctx.result("Could not delete patient information");
    }
}
