package dev.tan;

import dev.tan.controllers.*;
import io.javalin.Javalin;
import io.javalin.http.UnauthorizedResponse;
import org.slf4j.*;
import static io.javalin.apibuilder.ApiBuilder.*;

public class Server {

    private static Logger logger = LoggerFactory.getLogger(Server.class);

    public static void main(String[] args) {
        Javalin app = Javalin.create().start(7000);

        app.routes(() -> {
            path("/", () -> get(ctx -> ctx.result("Hello")));

            path("/register", () -> {
                post(AuthController::handleCreateAccount);
            });

            path("/login", () -> post(AuthController::handleLogin));

            path("/patients", () -> {
                before(AuthController::handlePreAuthCheck);
                get("search", PatientsController::handleSearchPatients);
                post("add", PatientsController::handleAddPatient);
                patch("edit", PatientsController::handleEditPatient);
                delete("delete", PatientsController::handleDeletePatient);
            });
        });
    }
}
